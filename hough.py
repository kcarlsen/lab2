import cv2
import numpy as np
import matplotlib.pyplot as plt

#Get Image
img = cv2.imread('/home/pi/Documents/lab2/checkerboard_bright.jpg')

#Display base image
plt.imshow(img)
plt.show()

#Perform Perspective transform
#pts1 = np.float32([[473,160], [1276,151], [34,583], [1599,573]])
#pts2 = np.float32([[0,0], [1150,0], [0, 1150], [1150, 1150]])

#M = cv2.getPerspectiveTransform(pts1, pts2)

#img = cv2.warpPerspective(img, M, (1150,1150))

#Show Corrected image
plt.imshow(img)
plt.show()

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
plt.imshow(gray)
plt.show()
edges = cv2.Canny(gray,50,150,apertureSize = 3)

#Display canny image
plt.imshow(edges)
plt.show()

#Prepare file object
f = open('theta_untransform.txt', 'w')

lines = cv2.HoughLines(edges,1,np.pi/180,200)
lines = lines[:,0,:]
for rho,theta in lines:
    f.write(str(theta) + '\n')
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + 1400*(-b))
    y1 = int(y0 + 1200*(a))
    x2 = int(x0 - 1400*(-b))
    y2 = int(y0 - 1200*(a))

    cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)

f.close()
#cv2.imwrite('houghlines3.jpg',img)
plt.imshow(img)
plt.show()
