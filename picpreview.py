import time
import picamera

def picPreview(filename):
    camera = picamera.PiCamera()
    camera.start_preview()
    time.sleep(5) #5 second preview delay
    camera.stop_preview()
    camera.capture(filename)
    
